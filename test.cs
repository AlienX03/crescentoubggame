﻿using UnityEngine;

public class NetworkManager : MonoBehaviour {
    public void ConnectToServer(User user) {
        Application.ExternalCall("Logon", JsonUtility.ToJson(user));
    }
    public void OnMatchJoined(string jsonresponse) {
        MatchJoinResponse mjr = JsonUtility.FromJson(jsonresponse);
        if (mjr.result) {
            Debug.Log("Logon successful");
        }
        else {
            Debug.Log("Logon failed");
        }
    }
    public void BroadcastQuit() => Application.ExternalCall("QuitMatch");
}