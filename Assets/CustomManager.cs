﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class CustomManager : MonoBehaviour
{

    private NetworkManager manager;
    
    // Runtime variable
    bool showServer = true;

    public NetworkManager Manager { get => manager; set => manager = value; }

    void Awake()
    {
        Manager = GetComponent<NetworkManager>();
        if (Application.isBatchMode)
        {
            Manager.StartServer();
            Debug.Log("Server Started");
        }

    }

    void Update()
    {
    }
}
